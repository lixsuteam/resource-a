package com.felixsu.resource.a.service;

import com.felixsu.commons.service.SimpleService;
import com.felixsu.resource.a.persistence.model.Movie;
import com.felixsu.resource.a.persistence.repository.MovieRepository;
import org.springframework.stereotype.Service;

/**
 * Created on 12/23/17.
 *
 * @author felixsoewito
 */
@Service
public class MovieService extends SimpleService<MovieRepository, Movie, Integer> {

    public MovieService(MovieRepository repository) {
        super(Movie.class, repository);
    }
}
