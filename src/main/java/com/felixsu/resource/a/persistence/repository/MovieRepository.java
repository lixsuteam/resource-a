package com.felixsu.resource.a.persistence.repository;

import com.felixsu.commons.persistence.BaseRepository;
import com.felixsu.resource.a.persistence.model.Movie;

/**
 * Created on 12/23/17.
 *
 * @author felixsoewito
 */
public interface MovieRepository extends BaseRepository<Movie, Integer> {
}
