package com.felixsu.resource.a.persistence.model;

import com.felixsu.commons.model.EntityModel;
import com.felixsu.commons.persistence.AuditBase;
import com.felixsu.commons.persistence.AuditListener;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created on 12/23/17.
 *
 * @author felixsoewito
 */
@Entity
@EntityListeners(AuditListener.class)
@Table(name = "movies")
@DynamicUpdate
public class Movie extends AuditBase implements EntityModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column
    private String description;

    @Column
    private String genre;

    @Column
    private String author;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
