package com.felixsu.resource.a.controller;

import com.felixsu.commons.controller.PartialUpdateController;
import com.felixsu.resource.a.persistence.model.Movie;
import com.felixsu.resource.a.persistence.repository.MovieRepository;
import com.felixsu.resource.a.service.MovieService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on 12/23/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("movie")
public class MovieController extends PartialUpdateController<MovieService, MovieRepository, Movie, Integer> {

    public MovieController(HttpServletRequest request, MovieService service) {
        super(request, service);
    }
}
